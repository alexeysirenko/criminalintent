package com.bignerdranch.android.criminalintent;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Oleksiy on 20.02.2016.
 */
public class ImagePreviewFragment extends DialogFragment {

    private static final String ARG_IMAGE_FILE = "image_file";

    public static final String EXTRA_IMAGE_FILE = "com.bignerdranch.android.criminalintent.image_file";

    private ImageView mImageView;

    public static ImagePreviewFragment newInstance(File imageFile) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_IMAGE_FILE, imageFile);
        ImagePreviewFragment fragment = new ImagePreviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_image_preview, null);

        File imageFile = (File) getArguments().getSerializable(ARG_IMAGE_FILE);
        if (imageFile != null) {
            mImageView = (ImageView) v.findViewById(R.id.dialog_image_image_preview);
            mImageView.setImageBitmap(PictureUtils.getScaledBitmap(imageFile.getPath(), getActivity()));
        }

        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle(R.string.image_preview_title)
                .create();
    }
}
